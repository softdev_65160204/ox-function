/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.oxfunction;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class OXFunction {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row, col;
    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            intputRowCol();
            if(isWin()) {
                printTable();
                printWin();
                break;
            } else if(checkX1()) {
                printTable();
                printWin();
                break;
            } else if(checkX2()) {
                printTable();
                printWin();
                break;
            } else if(isDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to OX");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    private static void intputRowCol() {
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.print("Please input row, col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if(table[row-1][col-1]=='-'){
                table[row - 1][col - 1] = currentPlayer;
                return;
            } else {
                System.out.println("Invalid input, Please try again");
            }
        }
    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if(checkRow()) {
            return true;
        } else if(checkCol()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(currentPlayer + " Win!!");
    }

    private static boolean checkRow() {
        for(int i=0; i<3; i++) {
            if(table[row-1][i]!= currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for(int i=0; i<3; i++) {
            if(table[i][col-1]!= currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if(table[0][0]==currentPlayer && table[1][1]==currentPlayer && table[2][2]==currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if(table[0][2]==currentPlayer && table[1][1]==currentPlayer && table[2][0]==currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++) {
                if(table[i][j]=='-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Game is Draw!!");
    }
}
